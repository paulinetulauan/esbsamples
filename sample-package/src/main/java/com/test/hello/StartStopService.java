package com.test.hello;

import org.slf4j.LoggerFactory;

import com.toro.esb.core.util.ESBLogger;

public class StartStopService {

    protected final ESBLogger logger = new ESBLogger( LoggerFactory.getLogger( getClass() ) );

    public void start() {
        logger.warnIfEnabled( "start service called" );
    }

    public void stop() {
        logger.warnIfEnabled( "stop service called" );
    }
}