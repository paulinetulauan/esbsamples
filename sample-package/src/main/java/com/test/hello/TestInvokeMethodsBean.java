package com.test.hello;

public class TestInvokeMethodsBean {

    private String name1 = "";

    private String name2 = "";

    private String address1 = "";

    private String address2 = "";

    private String city = "";

    private String state = "";

    private String country = "";

    private String postCode = "";


    public String getName1() {
        return name1;
    }

    public void setName1( String name1 ) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2( String name2 ) {
        this.name2 = name2;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1( String address1 ) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2( String address2 ) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity( String city ) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState( String state ) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry( String country ) {
        this.country = country;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode( String postCode ) {
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return "TestInvokeMethodsBean{" + "name1='" + name1 + '\'' + ", name2='" + name2 + '\'' + ", address1='" + address1 + '\'' + ", address2='" + address2 + '\'' + ", city='" + city + '\'' + ", state='" + state + '\'' + ", country='" + country + '\'' + ", postCode='" + postCode + '\'' + '}';
    }
}
