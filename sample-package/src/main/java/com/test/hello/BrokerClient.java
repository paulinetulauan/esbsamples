package com.test.hello;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class BrokerClient {

    private String topicName = "com.toro.commerce.Customer";

    public static void main( String[] args ) {
        BrokerClient subscriber = new BrokerClient();
        subscriber.subscribeWithTopicLookup();
    }

    public void subscribeWithTopicLookup() {

        TopicConnection topicConnection = null;
        try {
            String connectionString = "tcp://127.0.0.1:61616";
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory( connectionString );
            topicConnection = connectionFactory.createTopicConnection();

            System.out.println( "Create Topic Connection for Topic " + topicName );

            while ( true ) {
                try {
                    TopicSession topicSession = topicConnection.createTopicSession( false, Session.AUTO_ACKNOWLEDGE );

                    Topic topic = ( Topic ) topicSession.createTopic( topicName );
                    // start the connection
                    topicConnection.start();

                    // create a topic subscriber
                    javax.jms.TopicSubscriber topicSubscriber = topicSession.createSubscriber( topic );

                    TestMessageListener messageListener = new TestMessageListener();
                    topicSubscriber.setMessageListener( messageListener );

                    Thread.sleep( 5000 );
                    topicSubscriber.close();
                    topicSession.close();
                } catch ( JMSException e ) {
                    e.printStackTrace();
                } catch ( InterruptedException e ) {
                    e.printStackTrace();
                }
            }

        } catch ( JMSException e ) {
            throw new RuntimeException( "Error in JMS operations", e );
        } finally {
            if ( topicConnection != null ) {
                try {
                    topicConnection.close();
                } catch ( JMSException e ) {
                    throw new RuntimeException( "Error in closing topic connectiown", e );
                }
            }
        }
    }

    public class TestMessageListener implements MessageListener {

        public TestMessageListener () {
        }
        
        public void onMessage( Message message ) {
            try {
                if ( message instanceof ObjectMessage ) {
                    System.out.println( "Got the Message : " + ( ( ObjectMessage ) message ).getObject() );
                } else
                    System.out.println( "Got the Message : " + ( message ) );
            } catch ( Exception e ) {
                e.printStackTrace();
            }
        }
    }

}