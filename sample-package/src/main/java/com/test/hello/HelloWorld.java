package com.test.hello;

import javax.jms.Message;
import javax.jms.MessageListener;

public class HelloWorld implements MessageListener {
	
	public void test() {
		System.out.println( "hello world" );
	}
	
	public void sayHello( String name ) {
		System.out.println( "hello world to '" + name + "'" );
	}
	
	public String helloResponse() {
   	 String response = "<h3>Hello world method \'helloResponse()\' triggered successfully!</h3>";
   	 return response;

   }
	
	public void onMessage( Message message ) {
		System.out.printf( "[Received]\t%s", message.toString() );
	}
}
