package com.test.hello;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.toro.esb.core.exception.ToroException;

public class TestInvokeMethods {

    public TestInvokeMethods() {
        System.out.println( "constructor called" );
    }

    public static void staticNoArgs() {
        System.out.println( "staticNoArgs()" );
    }

    public void notStaticNoArgs() {
        System.out.println( "notStaticNoArgs()" );
        notStatic1( 1.2d, 2.3f, "" );
    }

    public void staticNoArgsException() throws Exception {
        throw new ToroException( "daren was here" );
    }

    public double notStatic1( double d, float f, Object o ) {
        return d + f;
    }

    public String notStatic1( double d, float f, String o2 ) {
        return o2 + "!!!   " + ( d + f );
    }

    public String array2String( String[] strs ) {
        String val = "";
        for ( String str : strs ) {
            val += str;
        }
        return val;
    }

    public String list2String( List<String> strs ) {
        String val = "";
        for ( String str : strs ) {
            val += str;
        }
        return val;
    }

    public String set2String( Set<String> strs ) {
        String val = "";
        for ( String str : strs ) {
            val += str;
        }
        return val;
    }

    public String collection2String( Collection<String> strs ) {
        String val = "";
        for ( String str : strs ) {
            val += str;
        }
        return val;
    }


    public TestInvokeMethodsBean getBean( String name1, String name2, String address1, String address2 ) {
        TestInvokeMethodsBean b = new TestInvokeMethodsBean();
        b.setName1( name1 );
        b.setName2( name2 );
        b.setAddress1( address1 );
        b.setAddress2( address2 );

        return b;
    }

    public String processBean( TestInvokeMethodsBean timb ) {
        return timb.toString();
    }

    public String helloPerson( String name ) {
        return "Hello " + name;
    }

    public String helloPerson2( String firstName, String lastName ) {
        return "Hello " + firstName + " " + lastName;
    }

    public String nameAndAge( String name, int age ) {
        return "Hello " + name + " you are " + age + " years old";
    }

}
