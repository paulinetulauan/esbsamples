package com.toro.commerce.api.inventory.jms.model;

import java.io.Serializable;

public class InventoryItem implements Serializable {

    private String productCode = "";

    private String warehouseId = "";

    private int warehouseStockLevel;

    private int supplierStockLevel;

    private int supplierShipLeadTime;


    public String getProductCode() {
        return productCode;
    }

    public void setProductCode( String productCode ) {
        this.productCode = productCode;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId( String warehouseId ) {
        this.warehouseId = warehouseId;
    }

    public int getWarehouseStockLevel() {
        return warehouseStockLevel;
    }

    public void setWarehouseStockLevel( int warehouseStockLevel ) {
        this.warehouseStockLevel = warehouseStockLevel;
    }

    public int getSupplierStockLevel() {
        return supplierStockLevel;
    }

    public void setSupplierStockLevel( int supplierStockLevel ) {
        this.supplierStockLevel = supplierStockLevel;
    }

    public int getSupplierShipLeadTime() {
        return supplierShipLeadTime;
    }

    public void setSupplierShipLeadTime( int supplierShipLeadTime ) {
        this.supplierShipLeadTime = supplierShipLeadTime;
    }
}
