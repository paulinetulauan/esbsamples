package com.toro.commerce.api.inventory.jms.publisher;

import com.toro.esb.core.invoker.annotation.ValueOptions;
import com.toro.esb.core.jms.MultiJMSTopicPublisher;

import javax.jms.ObjectMessage;
import javax.ws.rs.DefaultValue;
import java.util.LinkedHashMap;

public class CommerceItemPublisher extends MultiJMSTopicPublisher {

    public PublishResponse publishStockChanged(
            @DefaultValue("<linked-hash-map>\n" + "  <entry>\n" + "    <string>event</string>\n" + "    <string>added</string>\n" + "  </entry>\n" + "  <entry>\n" + "    <string>customerIds</string>\n" + "    <linked-list>\n" + "      <string>test1</string>\n" + "      <string>test2</string>\n" + "      <string>test3</string>\n" + "    </linked-list>\n" + "  </entry>\n" + "</linked-hash-map>")
            LinkedHashMap item,
            @ValueOptions(value = { "com.toro.commerce.Customer", "com.toro.commerce.Inventory", "com.toro.commerce.Payment" })
            String topicName ) throws Exception {
        ObjectMessage om = publishObject( topicName, item, true );
        return new PublishResponse( "OK", topicName, item, om );
    }

}