package com.toro.commerce.api.inventory.jms.publisher;

import java.util.Map;

import javax.jms.Message;

public class PublishResponse {

    private String status;

    private String topicName;

    private Map messageContents;

    private Message jmsMessage;

    PublishResponse( String status, String topicName, Map messageContents, Message message ) {
        this.status = status;
        this.topicName = topicName;
        this.messageContents = messageContents;
        this.jmsMessage = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus( String status ) {
        this.status = status;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName( String topicName ) {
        this.topicName = topicName;
    }

    public Map getMessageContents() {
        return messageContents;
    }

    public void setMessageContents( Map messageContents ) {
        this.messageContents = messageContents;
    }
}
