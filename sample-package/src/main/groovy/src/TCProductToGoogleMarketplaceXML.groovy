import groovy.xml.XmlUtil
class TCProductToGoogleMarketplaceXML{

	public String parseTemplate(File file) {
		return parseProductXML(file.text);
	}

	private String parseProductXML(String TCProduct) {
		def test = new XmlSlurper(false,true).parseText( TCProduct).declareNamespace([soapenv:"http://schemas.xmlsoap.org/soap/envelope/",prod:"http://www.torocommerce.com/ProductService/"])
		def prod_request = test.Body.'prod:CreateProductRequest'
		def context = [
			//mapping TC product API- Google Data Feeds XML
			id : prod_request['prod:id'],
			item_description : prod_request['prod:description'],
			site_description : "This is a sample site",
			site_title : "E-Commerce",
			google_category : "Apparel &amp; Accessories &gt; Clothing &gt; Dresses",
			item_title : prod_request['prod:displayName'],
			site_link : "valid site link",
			item_link : prod_request['prod:url'],
			name : prod_request.'prod:attributes'['prod:name'],
			brand : prod_request.'prod:brand'['prod:displayName']
		]

		def sample = context.parse() { """<?xml version="1.0"?>
<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">
<channel>
<title>\$site_title</title>
<link>\$site_link</link>
<description>\$site_description</description>
<item>
<title>\$item_title</title>
<link>\$item_link</link>
<description>\$item_description</description>
<g:brand>\$brand</g:brand>
<g:image_link>http://paulinetulauan.wix.com/fashion#!Im-a-title/zoom/mainPage/imagejdb</g:image_link>
<g:price>25</g:price>
<g:google_product_category>\$google_category</g:google_product_category>
<g:product_type>Home &amp; Garden &gt; Kitchen &amp; Dining &gt; Appliances &gt; Refrigerators</g:product_type>
<g:link>http://paulinetulauan.wix.com/fashion#!Im-a-title/zoom/mainPage/imagejdb</g:link>
<g:condition>new</g:condition>
<g:id>\$id</g:id>
<g:availability>in stock</g:availability>
</item>
</channel>
</rss>""" }


		println  new XmlUtil().escapeXml(sample)
		'ftp://googleMerchant_username:googleMerchant_password@uploads.google.com/remotefile.xml'.send( sample );
		return sample
	}
}