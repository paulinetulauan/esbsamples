//process a supplier stock file - populate a simple table like below
/*
CREATE TABLE TORO_SUPPLIER_STOCK_LEVELS (
    "PRODUCT_CODE"          VARCHAR2(40 BYTE) NOT NULL,
    "SUPPLIER_STOCK_LEVEL"  NUMBER NOT NULL,
    PRIMARY KEY ("PRODUCT_CODE")
) nologging;

insert into toro_supplier_stock_levels (select distinct(product_code), 0 from toro_barcodes)
commit;
*/









def count = 0
def found = 0
def ignored = 0;
def tick = System.currentTimeMillis()
def sql = 'barcodes'.sql();

// jets
def jets = { ps ->
    new File('/media/harddisk/Downloads/jets-stock.xlsx').eachRecord(1) { record ->
        def result = sql.firstRow("select product_code from toro_barcodes where barcode = ?", record[3])
        if (result && result['PRODUCT_CODE']) {
            ps.addBatch(5, result['PRODUCT_CODE'])
            found++
        } else
            ignored++
        count++
        if (count % 100 == 0) println 'processed ' + count
    }
    return "inserted ${found}, ignored ${ignored} out of ${count} records in ${System.currentTimeMillis() - tick} ms"
}

// seafolly
def seafolly = { ps ->
    new File('/media/harddisk/Downloads/stockdata.csv').eachRecord { record ->
        def result = sql.firstRow("select product_code from toro_barcodes where barcode = ?", record[3])
        if (result && result['PRODUCT_CODE']) {
            ps.addBatch(5, result['PRODUCT_CODE'])
            found++
        } else
            ignored++
        count++
        if (count % 100 == 0) println 'processed ' + count
    }
    return "inserted ${found}, ignored ${ignored} out of ${count} records in ${System.currentTimeMillis() - tick} ms"
}

def baku = { ps ->
    new File('/media/harddisk/Downloads/baku_supplierlevel.csv').eachRecord(1) { record ->
        def result = sql.firstRow("select product_code from toro_barcodes where barcode = ?", record[5])
        if (result && result['PRODUCT_CODE']) {
            def stockLevel = Integer.parseInt(record[4])
            ps.addBatch(stockLevel < 0 ? 0 : stockLevel, result['PRODUCT_CODE'])
            found++
        } else
            ignored++
        count++
        if (count % 100 == 0) println 'processed ' + count
    }
    return "inserted ${found}, ignored ${ignored} out of ${count} records in ${System.currentTimeMillis() - tick} ms"
}

def bodyArt = { ps ->
    new File('/media/harddisk/Downloads/book2.xlsx').eachRecord(1) { record ->
        def result = sql.firstRow("select product_code from toro_barcodes where barcode = ?", record[3])
        if (result && result['PRODUCT_CODE']) {
            def stockLevel = Integer.parseInt(record[2])
            ps.addBatch(stockLevel < 0 ? 0 : stockLevel, result['PRODUCT_CODE'])
            found++
        } else
            ignored++
        count++
        if (count % 100 == 0) println 'processed ' + count
    }
    return "inserted ${found}, ignored ${ignored} out of ${count} records in ${System.currentTimeMillis() - tick} ms"
}


// animal supplies
def animalSupplies = { ps ->
    new File('/media/harddisk/Downloads/stockdata.txt').newReader('UTF-16LE').eachRecord(2, [32, 134, 176, 217, 255]) { record ->
        if (record[2] != 0) {
            def result = sql.firstRow("select product_code from toro_barcodes where barcode = ?", record[4])
            if (result && result['PRODUCT_CODE']) {
                ps.addBatch(record[2], result['PRODUCT_CODE'])
                found++
            } else
                ignored++
        } else
            ignored++
        count++
        if (count % 100 == 0) println 'processed ' + count
    }
    return "inserted ${found}, ignored ${ignored} out of ${count} records in ${System.currentTimeMillis() - tick} ms"
}


try {
    def output
    sql.withBatch(16000, 'update toro_supplier_stock_levels set supplier_stock_level = ? where product_code = ?') { ps ->
//        output = animalSupplies(ps)
//        output = jets(ps)
//        output = baku(ps)
//        output = seafolly(ps)
        output = bodyArt(ps)
    }
    return output
} finally {
    sql.close()
}





// load up database table with barcodes from inventory
/*
CREATE TABLE TORO_BARCODES (
    "ID"           NUMBER,
    "PRODUCT_CODE" VARCHAR2(40 BYTE),
    "NAME"         VARCHAR2(255 BYTE),
    "RETAILUNITS"  NUMBER,
    "PRINT"        VARCHAR2(200 BYTE),
    "LABEL"        VARCHAR2(200 BYTE),
    "BARCODE"      VARCHAR2(40 BYTE),
    "IS_DEFAULT"   NUMBER,
    PRIMARY KEY ("ID")
) nologging;

create index barcode on toro_barcodes(barcode);
*/

/*
def sql = 'barcodes'.sql();
def count = 0;
def tick = System.currentTimeMillis();
try {
    sql.withBatch(16000, 'insert into toro_barcodes (id, product_code, name, retailunits, print, label, barcode, is_default) values (?,?,?,?,?,?,?,?)') { ps ->
        new File('/tmp/barcodes.csv').eachRecord(1) {
            ps.addBatch(it[0], it[1], it[2], it[3], it[4], it[5], it[6], it[7])
            count++
            if (count % 1000 == 0) println 'processed ' + count
        }
    }
    return 'OK, completed in ' + (System.currentTimeMillis() - tick) + ' ms'
} finally {
    sql.close()
}

*/
