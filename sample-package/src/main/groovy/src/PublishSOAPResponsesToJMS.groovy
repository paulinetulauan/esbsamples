package com.toro.esb
/*
 *
 * publishingsoap-responseto-jms.groovy
 *
 * Using TORO ESB Groovy Methods:
 *
 *
 * ESBGroovyMethods.publishTo()
 *
 * This script invokes a webservice.
 * The result will be published to a JMS queue.
 *
 */

//Used to referenced artifact and its dependencies, making it available to the classpath
@Grab(group="com.github.groovy-wslite",module="groovy-wslite",version="0.8.1")
import wslite.soap.*
def publishCityForecastByZip(String zip){
	//Url where to send the SOAP message
	def soapUrl = 'http://wsf.cdyne.com/WeatherWS/Weather.asmx'
	def client = new SOAPClient(soapUrl)
	//Send soap request using the client.send method
	def response = client.send(SOAPAction: 'http://ws.cdyne.com/WeatherWS/GetCityForecastByZIP'){
			body{
				GetCityForecastByZIP('xmlns':'http://ws.cdyne.com/WeatherWS/'){
					ZIP(zip)
				}
			}
		}
	//Get the response data to print the Description of the forecast in the first element array[0].
	String weatherResult = response.GetCityForecastByZIPResponse.GetCityForecastByZIPResult.ForecastResult.Forecast.Desciption[0]
	 
	//Publish result to JMS using publishTo(queueName)
	//When queueName where you will publish result is not available it will automatically create new queueName
	def sample = weatherResult
	sample.publishTo('com.toro.weather.WeatherQeue')
}
publishCityForecastByZip("63104")
