/*
 *
 *
 * This script invokes a list customer webservice in the ESB and write the result in the .csv file.
 *
 *
*/
@Grab(group="com.github.groovy-wslite",module="groovy-wslite",version="0.8.1")
import wslite.soap.*
class customerServiceImpl {
	def soapUrl = 'Webservice uploaded in the ESB'
	def client = new SOAPClient(soapUrl)
	def String checkCustomer(def custid){
		def response = client.send(SOAPAction: 'http://www.torocommerce.com/customerservice/listCustomer'){
			body{
				ListCustomerRequest('xmlns':'http://www.torocommerce.com/customerservice/'){
					customerIdList(custid)
					customerSearch{
						firstName('Dexter')
						lastName('Jaen')
					}
				}
			}
		}
		def nodes =  response.ListCustomerResponse.customerList
		def childCount = nodes.children().size()
		new File("customer.csv").withWriter{ out ->
			nodes.children().each(){
				if(it.children().size()>0){
				it.children().each(){
					out.write(it.text() + ",")
					}
				}else
					out.write(it.text() + ",")
			}
		}
		new File("customer.csv").withReader { reader ->
			println reader.readLine()
		}
	}
	public static void main(String[] args){
		def cs = new customerServiceImpl()
		println cs.checkCustomer("customerOrderId")
	}
}