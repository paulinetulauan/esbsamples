import com.toro.esb.ESBGroovyMethods
@Grab( group = 'com.github.groovy-wslite', module = 'groovy-wslite', version = '0.8.0' )
import wslite.soap.*

class MagentoInventory {
    String getInventoryXml( ) {
        def client = new SOAPClient( 'http://192.168.200.107/magento/index.php/api/v2_soap/index/' )
        def soapUrl = 'http://192.168.200.107/magento/index.php/api/v2_soap/index/'
        def response;
        def sessionKey = '52e05e5a0038dec6b4a162edd7dbcfa0'
        println 'calling magento to get a session id'
        response = client.send( SOAPAction: 'http://192.168.200.107/magento/index.php/api/v2_soap/index/' ) {
            body {
                login( xmlns: 'Magento' ) {
                    username( 'devcsrj' )
                    apiKey( 'reijhanniel' )
                }
            }
        }
        sessionKey = response.envelope
        println "got a session id which is ${sessionKey}"

        response = client.send( SOAPAction: soapUrl ) {
            body {
                catalogProductList( xmlns: 'Magento' ) {
                    sessionId( sessionKey )
                }
            }
        }

        def ids = [];
        response.catalogProductListResponse.storeView.item.each() {
            ids.add( it.product_id )
        }

        response = client.send( SOAPAction: soapUrl ) {
            body {
                catalogInventoryStockItemList( xmlns: 'Magento' ) {
                    sessionId( sessionKey )
                    products() {
                        ids.each() {
                            item( it )
                        }
                    }
                }
            }
        }
        def output = "<inventoryItems>"

        response.catalogInventoryStockItemListResponse.result.item.each() {
            output += "    <inventoryItem>\n"
            output += "        <productCode>${it.product_id}</productCode>\n"
            output += "        <warehouseId>hardcoded</warehouseId>\n"
            output += "        <warehouseStockLevel>${it.qty}</warehouseStockLevel>\n"
            output += "        <supplierStockLevel>0</supplierStockLevel>\n"
            output += "        <supplierShipLeadTime>0</supplierShipLeadTime>\n"
            output += "    </inventoryItem>\n"
        }
        output += "</inventoryItems>"
        return output
    }


    String xmlToSql( String xml ) {
        ESBGroovyMethods esb = new ESBGroovyMethods()
        def rootNode = new XmlSlurper().parseText( xml )
        def output = "";
        rootNode.children().each {
            output += "insert into inventory_stage ( product_id, stock_level ) values ('${it.productCode}', '${it.warehouseStockLevel}')\n"

            def inventoryRecord = ['productCode': it.productCode.toString(), 'stockLevel': it.warehouseStockLevel.toString()]
            def map = ['action': 'inventory-update', 'data': inventoryRecord]
            esb.publishObjectMessage( 'com.toro.commerce.Inventory', map )

        }
        return output
    }


    void insertSql( String sql ) {
        ESBGroovyMethods esb = new ESBGroovyMethods()
        def database = esb.getSQL( 'tracker' )
        def queries = sql.split( "\n" );
        queries.each() {
            database.execute( it )
        }
        database.commit()
        database.close()
    }


    public String addCustomer( String name, String phone, String address ) {
        return 'hello'
    }



    static String process( ) {
        def mi = new MagentoInventory();
        def xml = mi.getInventoryXml()
        def sql = mi.xmlToSql( xml )
        mi.insertSql( sql )
    }
}