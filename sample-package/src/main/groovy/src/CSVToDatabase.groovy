package com.toro.esb

/*
 *
 * csv-to-database.groovy
 *
 * Using TORO ESB Groovy Methods:
 * 
 * ESBGroovyMethods.sql()
 * ESBGroovyMethods.file()
 * ESBGroovyMethods.eachRecord()
 *
 * This script retrieves data in the form of InputStream from CSV file located at VFS.
 * InputStream data will be parse and save it to the database.
 * 
 * @Author Rae Burawes
 * 
 */

import org.apache.commons.csv.CSVFormat
import groovy.sql.Sql

public class CSVToDatabase {

	public static void parseAttachment( InputStream stream, String dbPoolName ){

		//Establish database connection by calling one of the running ESB Databases, use 'poolName'
		def sql = dbPoolName.sql()

		//Drop existing table and create another one
		sql.execute('''
		DROP TABLE IF EXISTS xlsdata;
		CREATE TABLE xlsdata
		(
  		id bigserial NOT NULL,
  		data_name character varying(20),
  		data1 integer,
  		data2 integer,
  		status character varying(20),
  		CONSTRAINT xlsdata_pkey PRIMARY KEY (id )
		)
		WITH (
  		OIDS=FALSE
		);
		ALTER TABLE xlsdata OWNER TO postgres;''')

 		//Iterate on the data retrieve from CSV file and save it to the database.
		try {
			sql.withBatch( 100, "insert into xlsdata (data_name, data1, data2, status) values (?, ?, ?, ?)" ){ ps ->
				is?.eachRecord( 0 ,CSVFormat.RFC4180 ) {
					def data = [it[0],it[1].toInteger(),it[2].toInteger(),it[3]]
					ps.addBatch( data )
					println "processed " + data
				}
			}
			
		} finally {
			sql.close()
		}
				
	}
	
	static void main( String[] args ){
		//Change this to actual file location
	    def is = "ftp://username:password@host:21/path/to/csv/file".file().getInputStream()
		parseAttachment( is, "exceldb" );
	}
}
