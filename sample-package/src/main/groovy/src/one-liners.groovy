//send an email
"smtp[s]://login:password@server[:port]/subject".send([to: 'to-address'], "email body content");

// ftp a file
"[s]ftp[s]://login:password@server[:port]/directory1/directory2/directoryX/file".send("file content")

//copy a file - source and targets can be ftp/ftps/sftp/file
"[s]ftp[s]://login:password@server[:port]/directory1/directory2/directoryX/source".copyTo("file://home/daren/target.txt")

//publish to a queue
'com.toro.commerce.Customer'.publishString('message content')

//or the other way around - publish content to a queue
'message content'.publishTo('com.toro.commerce.Customer')

//list contents of an ftp directory
def children = "[s]ftp[s]://login:password@server[:port]/directory1/directory2/directoryX/".file().getChildren();
children.each {
    println 'folder is ${it}'
}

// hit a http url
def response1 = "http[s]://[login:password]@server[:port]/directory/file".http()
def response2 = "http[s]://[login:password]@server[:port]/directory/file".http([a: 'b', c: 'd'])
def response3 = "http[s]://[login:password]@server[:port]/directory/file?a=b&c=d".http()

//parse a velocity template, context as variable
def template = 'hello $name'.parse([name: "world"])

//parse a velocity template, context as closure
def template2 =  'hello $name'.parse() {
    [name: "world", foo: "Foo", bar: "Bar"]
}

// or  the other way around - put the template in a closure
def template3 = [name: "daren", surname: "Klamer"].parse {
    'hello $name $surname'
}

//or put the template in as a parameter
def template4 = [name: "daren", surname: "Klamer"].parse('hello $name $surname')