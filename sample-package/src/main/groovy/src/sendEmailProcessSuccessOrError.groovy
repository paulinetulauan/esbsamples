/*
 *
 *
 *
 *
 * Using TORO ESB Groovy Methods:
 *
 * ESBGroovyMethods.send()
 *
 *
 * This script send a string of data on Email.
 *
 *
 */
try{
	//service invoked
	String success = "Service successfully invoked"
	//throw new Exception("Some random error");
	'smtps://email%40gmail.com:password@smtp.gmail.com:587/subject'.send( [to: email ], success );

}catch ( Exception e ) {
	def error = e.message
	'smtps://email%40gmail.com:password@smtp.gmail.com:587/subject'.send( [to: email ], e.toString() );
}

