class createEmailUsingVelocityTemplate {
	
		public String parseTemplate(File file) {
			return parseVelocityEmail(file.text);
		}
		
		private String parseVelocityEmail(String velocity) {
			def map = [salutation: "Ma'am", lastName : "Tulauan" , userName : "pau" , fullName : "Pauline Angelique P. Tulauan", address: "NIA CLSU Nueva Ecija", phoneNumber: "09091234566", adminFullName:"Jerrick Pua", adminCompany:"Trixan"]
			def sample = velocity.parse( map )
			return sample;
		}
	}
	