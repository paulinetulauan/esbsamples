def tomcat = 'webManager'.bean()
def packageManager = 'packageManager'.bean()

new Thread(new Runnable() {
    @Override
    void run() {
        Thread.sleep(1000)
        println 'about to shut down tomcat'
        packageManager.destroy()
        tomcat.destroy()
        println 'tomcat down, shutting down vm'
        System.exit(0)
    }
}).start();