package com.toro.esb

import com.toro.esb.ESBGroovyMethods
import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.internet.MimeMessage
import org.apache.commons.mail.util.MimeMessageParser

/*
 * Retrieve CSV or Excel file attachments from email using 'receive()' method of ESBGroovyMethods
 * Use CSVToDatabase to read the contents of the attachment and save it to the database.
 * @param String url
 * 
 * @Author Rae Burawes
 *
 */

def username = URLEncoder.encode( 'username', 'UTF-8' )
def password = URLEncoder.encode( 'password', 'UTF-8' )
def port = '993'
def folder = 'INBOX'

//Construct url to be passed as parameter to 'receive()' method
def url = "imaps://$username:$password@imap.gmail.com:$port/$folder"

//Retrieve messages by using ESBGroovyMethods 'receive'
def messages = ESBGroovyMethods.receive( url )

//Loop on each message 
messages.each { msg ->
    
    if ( msg instanceof MimeMessage ) {
        
        //Create message parser
        def parser = new MimeMessageParser( ( MimeMessage ) msg ).parse();
        def attachments = parser.getAttachmentList()
        
        //Loop on attachments
        attachments.each { 
            def filename = it?.name
            println "processing ... " + filename
            //
            if ( filename?.contains( ".csv" ) || filename?.contains( ".xls" ) ) {
                def is = it?.getInputStream()
                //Use 'parseAttachment()' method to read the content of the file
                CSVToDatabase.parseAttachment( is, "exceldb" )
            }
        }
    }
    
}