package com.toro.esb

/*
 *
 *
 * invokes-webservice.groovy
 *
 * Using TORO ESB Groovy Methods:
 *
 * ESBGroovyMethods.invoke()
 *
 *
 * This script invokes a webservice.
 *
 *
*/
 import com.toro.esb.core.http.ESBHttpClient

def convertRate(def from, def to){
	def soapUrl = 'http://www.webservicex.net/CurrencyConvertor.asmx'
	def soap = new ESBHttpClient()
	//Post connection to the webservice to send request using invoke(uri,params,header and body)
	def response = soap.invoke(soapUrl, [:], [ "Content-Type" : "text/xml", "SOAPAction" : "http://www.webserviceX.NET/ConversionRate" ],
		"""<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:web="http://www.webserviceX.NET/">
			<soap:Header/>
				<soap:Body>
					<web:ConversionRate>
						<web:FromCurrency>GBP</web:FromCurrency>
						<web:ToCurrency>PHP</web:ToCurrency>
					</web:ConversionRate>
				</soap:Body>
		</soap:Envelope>"""
		)
	//Parse the XML response
	//@see http://groovy.codehaus.org/Reading+XML+using+Groovy's+XmlParser
	def ns = new groovy.xml.Namespace("http://schemas.xmlsoap.org/soap/envelope/", 'soap')
	def records = new XmlParser().parseText(response)
	//Will print the response
	println records['soap:Body']['ConversionRateResponse']['ConversionRateResult'].text()
}
convertRate("GBP","PHP")