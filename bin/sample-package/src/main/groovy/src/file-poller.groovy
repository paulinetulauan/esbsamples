class FilePoller {

    public void methodWithReader(Reader reader) {
        println "method with Reader called: ${reader}"
    }

    public void methodWithReader(InputStream inputStream) {
        println "method with IS called: ${reader}"
    }

    public void methodWithStrings(String internalId, Reader reader) {
        println "method with String called: ${reader}"
    }

    public String methodWithBytes(byte[] bytes, String internalId, String filename) {
        println "method with bytes called: " + new String(bytes) + " and internal id ${internalId} and filename ${filename}"
        return 'service worked'
    }

    public String publishFileToJMSAgain(File file, byte[] bytes) {
        println 'pushing message'
        'com.toro.commerce.Customer'.publishString(new String(bytes))
        //"ftp://rj.campos:iamreijhanniel@192.168.22.27/input.txt".copy("ftp://rj.campos:iamreijhanniel@192.168.22.27/input2.txt");
        println 'pushing message done'
        return 'OK'
    }

    public String methodWithFile(File file) {
        println "got a file ${file.name}"
        file.eachLine {
            println it // Evaluates the line and prints the version
        }
        return "hello world, return value is ${file}"
    }

    public String convertFileToUpper(File file) {
        def output = '';
        file.eachLine {
            output += it.toUpperCase() + '\n'
        }
        println output
        return output
    }

    public String helloUpper(String text, String internalId) {
        internalId.addDocumentLog("daren was here")
        return "hello ${text.toUpperCase()} ${internalId}"
    }
}
